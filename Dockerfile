#local: docker build -t api-proxy:latest .
FROM ubuntu:latest

RUN apt-get update && \
apt-get install -y nginx vim

RUN mkdir -p /data/nginx/cache

COPY nginx/default /etc/nginx/sites-available

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]